# Description:
#   Tells USER to SENTENCE
# 
# Commands:
#   hubot tell @USER (in #room) to SENTENCE - Tells USER to SENTENCE 

module.exports = (robot) ->
  robot.respond /tell (@?\w+) (in )?(\#\w+)?( to)?(.*)/, (msg) ->
    if msg.match[3]
        robot.messageRoom "#{msg.match[3]}", msg.match[1] + ": " + msg.match[5]
    else 
        msg.send msg.match[1] + ": " + msg.match[5]
