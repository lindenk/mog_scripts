spawn = require('child_process').spawn

module.exports = (robot) ->
  robot.respond /.*what'?s your ip\??/i, (msg) ->
    ipcmd = spawn 'curl', ['http://ipecho.net/plain']
    ipcmd.stdout.on 'data', (data) -> 
      msg.reply data.toString()
    ipcmd.stderr.on 'data', (data) ->
      msg.send data.toString()
