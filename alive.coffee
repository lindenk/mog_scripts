# Description:
#   Simple script to reply when asked if alive
#
# Commands:
#   hubot you alive? - Responds if alive

module.exports = (robot) ->
  robot.respond /.*you alive.*\?/i, (msg) ->
    msg.emote "checks pulse..."
    
    setTimeout () ->
      msg.reply "Yep!"
    , 1500
