
module.exports = (robot) ->
  robot.respond /(now )?make me a sandwich/i, (msg) ->
    if msg.message.user.name == "lindenk"
      robot.emit "google-images:image", msg, "sandwich"
    else
      robot.reply "Make it yourself!"

  robot.hear /pets? @?mog/i, (msg) ->
    if msg.message.user.name == "lindenk"
      msg.send "_prrrrrrr...._"
    else
      msg.reply "_HISSSSSSSSS!!_"
