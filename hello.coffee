
module.exports = (robot) ->
  helloReplys = ["Hello thar!", "Hi!", "Ugh, its you..."]

  robot.respond /hello!?|hey!?|hi!?|what'?s up\??|howdy!?/i, (msg) ->
    msg.reply msg.random helloReplys
