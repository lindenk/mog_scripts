# Description:
#   Give a totally unbiased rated to the asker
#
# Commands:
#   hubot rate me - Gives a totally unbiased rating to the asker

module.exports = (robot) ->
  robot.respond /rate me/i, (msg) ->
    rating = Math.floor (Math.random() * 11)
    
    if rating >= 8
      msg.reply "Your a #{rating}! You sexy beast you ;)"
    else if rating >= 5
      msg.reply "Your a #{rating}. Meh..."
    else if rating >= 2
      msg.reply "Your a #{rating}. Might want to work on your....everything...."
    else
      msg.reply "#{rating}, now go away."

  robot.respond /seduce me/i, (msg) ->
    msg.reply "I'm not one of your Fried-Chicken Tramps!"
