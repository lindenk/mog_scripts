# Description:
#   Play a game of blackjack! (Currently WIP)
#
# Commands:
#   hubot draw - Draw a card from the deck
#   hubot shuffle - Shuffle all cards back into the deck

_ = require 'underscore'

suits = ['\u2663','\u2665','\u2666','\u2660']
values = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K']

fulldeck = _.flatten ({suit: x, value: y} for x in suits for y in values)

reshuffle = () ->
  deck = fulldeck.slice 0

  for i in [0..deck.length-1]
    # Swap with random element
    j = Math.floor Math.random() * (i + 1)
    [deck[i], deck[j]] = [deck[j], deck[i]]

  return deck
      

module.exports = (robot) ->
  deck = reshuffle()

  robot.respond /draw/i, (msg) ->
    if deck.length <= 0
      msg.send "Shuffling deck..."
      deck = reshuffle()

    card = deck.pop()
    msg.reply "#{card.value}#{card.suit}"

  robot.respond /shuffle/i, (msg) ->
    msg.send "Shuffling deck..."
    deck = reshuffle()
