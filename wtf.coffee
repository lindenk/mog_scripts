# Description:
#   Responds to "wtf"
#
# Commands:
#   hubot wtf - Sends a witty response.

module.exports = (robot) -> 
  wtfResponses = ["Hey! I just did what you guys told me to! >.<", "Sorry... :(", "Sorry... ;)"]

  robot.respond /wtf\??/i, (msg) ->
    msg.send msg.random wtfResponses
