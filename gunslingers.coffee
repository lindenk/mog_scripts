# Description:
#   Prints a picture of a gunslinger whenever it is mentioned alone

module.exports = (robot) ->
  robot.respond /^\s*g+u+n+s+l+i+n+g+e+r+s*!*\s*$/i, (msg) ->
    msg.send "http://caseyzemanonline.com/wp-content/uploads/2010/10/billythekid.jpg"
