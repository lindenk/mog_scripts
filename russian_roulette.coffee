# Description:
#   Do you feel lucky?
#
# Commands:
#   hubot setup roulette - Starts waiting for people to join a game of russian roulette
#   hubot start roulette - Starts the game of roulette
#   hubot stop roulette  - Ends the game of russian roulette

shuffleArray = (arr) ->
  for i in [0..arr.length-1]
    # Swap with random element
    j = Math.floor Math.random() * (i + 1)
    [arr[i], arr[j]] = [arr[j], arr[i]]

  return arr

module.exports = (robot) ->
  promptForTurn = (msg) ->
    player = game.players.shift()
    game.players.push(player)
    msg.send "#{player} is up! (Type 'pull' to take your turn)"

  game = undefined

  robot.respond /setup (russian )?roulette( [0-9]+)?( [0-9]+)? ?(#\w+)?/i, (msg) ->
    if game?
      msg.reply "Game already ongoing. Use 'stop roulette' to end game early."
      return

    shots = 6
    if msg.match[2]
      shots = Number(msg.match[2])

    bullets = 1
    if msg.match[3]
      bullets = Number(msg.match[3])

    game =
      shots: shots
      bullets: bullets
      players: []
      state: 'wait'

    introStr = "Russian roulette setup by #{msg.message.user.name} with a chamber size of #{shots} and #{bullets} bullets. Type '@mog join' to join."

    if msg.match[4]
      robot.messageRoom msg.match[4], introStr
    else
      msg.send introStr

  robot.respond /stop (russian )?roulette/i, (msg) ->
    if game?
      game = undefined
      msg.send "Ending game of russian roulette as per #{msg.message.user.name}'s request"
    else
      msg.reply "Game is not currently running."

  robot.respond /join/i, (msg) ->
    if game? and game.state == 'wait' 
      if msg.message.user.name in game.players
        msg.reply "You are already in the game."
        return

      game.players.push msg.message.user.name
      msg.send "Added #{msg.message.user.name} to game"

  robot.respond /start roulette/i, (msg) ->
    if game? and game.state == 'wait'
      if game.players.length < 1
        msg.reply "Cannot start a game with no players"
        return
      if game.players.length < game.bullets
        msg.reply "Cannot start a game with less players than bullets: #{game.players.length} players, #{game.bullets} bullets"
        return

      game.state = 'running'
      #msg.send "Attempting to shuffle players..."
      game.players = shuffleArray game.players
      msg.send "Starting game with players: #{game.players.toString().replace(/,/g,' ')}"
      msg.send "Game starting..."

      setTimeout () ->
        promptForTurn(msg)
      , 3000

  robot.hear /pull( [0-9]+)?/i, (msg) ->
    if game? and game.state == 'running'
      game.state = 'pulling'
      if msg.message.user.name != game.players[game.players.length-1]
        msg.reply "It's not your turn, you suicidal maniac!"
        return

      isDead = Math.floor Math.random() * game.shots < game.bullets
      if isDead
        msg.send "BANG!"
        game.bullets -= 1
        setTimeout () ->
          msg.send "#{msg.message.user.name} has shot themself!"
          game.players.pop()

          if game.bullets <= 0
            if game.players.length > 0
              msg.send "The game is over! #{game.players.toString().replace(/,/g,' ')} win!"
            else
              msg.send "The game is over. You lose!"
            game = undefined
          else
            promptForTurn(msg)
            game.state = 'running'
        , 2000
        return

      msg.send "Click."
      game.shots -= 1
      setTimeout () ->
        msg.send "It looks like you've survived! There are #{game.shots} pulls and #{game.bullets} bullets remaining."
        setTimeout () ->
          promptForTurn(msg)
          game.state = 'running'
        , 2000
      , 2000
