# Description:
#   Completes sentences ending with stop and stahp

module.exports = (robot) ->
  robot.respond /.*stop[\.!\?]?$/i, (msg) ->
    msg.send "Hammertime!"
    robot.emit "google-images:image", msg, "hammertime"
  
  robot.respond /.*stahp[\.!\?]?$/i, (msg) ->
    msg.send "Pajama Time!"
    robot.emit "google-images:image", msg, "pajama time"
